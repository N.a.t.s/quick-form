import {
  Button, Container,

  TextField,
  Typography
} from '@material-ui/core';
import { GoogleSpreadsheet } from "google-spreadsheet";
import React, { useState } from 'react';

const PKEY = '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCu9Qbt6dUirWh+\ni66L8QPgLOcNvicg1L7BdIJ71fI3zGpNdAtQiqqLIWbgGaQiR/ttFdyWIzOOmcOt\nnlXhlmQ3W6rrNEhGf/O91TscLUbidwA1A68pKQ2aA7BIN9KKhx/rYgs4WA+FJ0Z3\n0vBUQVFQrvD5PT+BZK9PFbFFtZkIFNCp6NNfqVMuwwMznkprqRBBHutYf7WE5op1\nKsHJu/lWKhZOG57No7fLecgUJ5o0to3NW/BzBnMamFfIuQZei++lBT+BbyfNiWzk\nDG4rtYwM7uB7fmGlsTxnRu/hLlex24Mk8PeQe2WQqj1MccAUKr50FFvEv/mis3MU\nLCZFZK6NAgMBAAECggEABy/tKjJgLVXC1GoAEGMlBd1KW5vrZxOeR3u2DYjHM8gb\nuNVSzqr3oRIM9BlRjw/2ce+DOY62JD5IVdZKkGMZeFW1coaaDn1cobiD/l8UyaJa\nWZMua6oyUxiiMgFLDqJI6yZCsDyb3Jw1rp/1e/yX7tDU+OQteu8MLZtGRkcUw4pn\n6QWra9SKoYhcko06dO0cQe32xnurMQOeWUCBfhHc8ervmd/yVBbkA706lWGhkDxR\nJpqbugnYaxDWDMJF2leRGt9OW1PKIbWiCAjpzvh7CHRAlAlCUAl2rpKR1+TIqzBp\nOl507Q2RdPhU6raCAhGbQHYlTLlDKMi3ipN3zIZKEQKBgQDmwPW88689+AvXBWuU\nzbMjDuAArUbWwP8gESoRgexf9lhRzziMcz8WRH9HWVlQlkMeinfveGmALz3+NlQE\n3e8qRplJKGMFQEG31EiPF4xxyTHsxAID5QSr8froWC9NctKbVBnATiQN5+FN2VdP\nun9mzOZoCsgTtihgduT2uSNq3QKBgQDCGUtJb7ZufnvT7GqSnCvfEmr4af9NPWWJ\n7K5wLyY/FeZmWl5nqsrvfKNrWSdwBfceQQkaqZdqBnPYn7MuX3VKcy7e0yBMJyF1\nhupTVaYGFqOt6y50yIQtwPBrnlTTt0oYl8tTW01hi3huOJuSH3b3UpvTK9qTHzaI\nVDPGpJzfcQKBgQCYC8cgNPVggMPUb7beetexnwiMiaBmuw6gY47zsEc/SAQUC9W3\n/eww0PuI2N/YENqzEgxLF2100CwqCEQ+XLpNge1Y3iq4+AkVhuQ0nRCmnSqvVsv5\nIvt8DNsGI60vfBw3yyeYpiRtoT+82xwD5eAyrYqYPZpuGpXqUg02YPFsZQKBgA+T\nTgpPj5kgRzmDUC4BoDefV7wlbVo6YgqGVp0j+3K2hC2UdNzmxLAhFVAI8HQ51lG0\nMFHWaBMW/3bToeJHwLa/tXQmBwJ1ZWbz85vKVK9KY8oSBEFU0d6GDATmyLHQlH6S\nt7sxqWz5bymRG1LHeZlxJJX2f+ysZYjdEOaQ0RHRAoGBALLSVmHRt5umfZwkIorV\nQDwX8fyjIEg7+BGUnnZUJkWnaui8HwJ3om1dbDr9aD1quifmYWOpleo3Y+xfiRv1\neyFY+n361kpmyzXu/oSyodmN3SDIV6AWWu5WJ+ojutpyz9A1JiozQL9GZoKwygpf\nWAuRe5zSrGS4kkMRdqIaPzuz\n-----END PRIVATE KEY-----\n'
const CEMAIL = 'admin-945@quick-form-312318.iam.gserviceaccount.com'
const SSHEETID = '1e80mlHbLLuONj_3oCzt9AFuSRzcnQcbkF-L39nznOyk'
const SHEETID = '0'

const App = () => {
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [status, setStatus] = useState('');

  const doc = new GoogleSpreadsheet(SSHEETID);

  const appendSpreadsheet = async (row) => {
    try {
      await doc.useServiceAccountAuth({
        client_email: CEMAIL,
        private_key: PKEY,
      });
      await doc.loadInfo();
  
      const sheet = doc.sheetsById[SHEETID];
      const result = await sheet.addRow(row);
      setStatus('succes')
    } catch (e) {
      setStatus('fail')
    }
  };
  
  const submit = () => {
    const newRow = { 'Firstname': firstname, 'Lastname': lastname, 'Email': email };
    appendSpreadsheet(newRow);
    setFirstname('')
    setLastname('')
    setEmail('')  
  }

  return (
    <div className="App">
      <Container style={{ display:'flex', flexDirection:'column'}}>
        <TextField id="firstname_field" label="Ім'я / Prénom" value={firstname} onChange={(e) => { setFirstname(e.target.value) }}/>
        <TextField id="lastname_field" label="Прізвище / Nom" value={lastname} onChange={(e) => { setLastname(e.target.value) }}/>
        <TextField id="email_field" label="Електронна пошта / Email" value={email} onChange={(e) => { setEmail(e.target.value) }}/>
        {status === 'succes' && <div style={{justifyContent: 'center', backgroundColor: 'green', color: 'white'}}><Typography>Успіх</Typography></div>}
        {status === 'fail' && <div style={{justifyContent: 'center', backgroundColor: 'red', color: 'white'}}><Typography>Успіх</Typography></div>}
        <Button onClick={() => { submit()}}>
          Підтвердити
        </Button>
      </Container>
    </div>
  );
}

export default App;
